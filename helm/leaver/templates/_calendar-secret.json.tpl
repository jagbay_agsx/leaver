{
  "installed": {
  	"client_id": {{ .Values.config.calendar.clientId }},
    "project_id": {{ .Values.config.calendar.projectId }},
    "auth_uri": {{ .Values.config.calendar.authUri }},
    "token_uri": {{ .Values.config.calendar.tokenUri }},
    "auth_provider_x509_cert_url": {{ .Values.config.calendar.authProvider }},
    "client_secret": {{ .Values.config.calendar.clientSecret }},
    "redirect_uris": {{ .Values.config.calendar.redirectUri }}
  }
}
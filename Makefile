######################### START CONFIG ##########################
#
# INSTRUCTIONS: Change the appropriate values in this area.
# Don't change anything beyond this block if not deemed necessary
#
#################################################################

HARBOR_REGISTRY=harbor.amihan.net
PROJECT=leavenotification
APP=leaver
TAG=latest

BUILD_IMAGE=${HARBOR_REGISTRY}/${PROJECT}/${APP}:${TAG}
CHART_REPO=https://${HARBOR_REGISTRY}/chartrepo/${PROJECT}

HARBOR_USER=<changeme>
HARBOR_PASS=<changeme>

LDFLAGS=-ldflags "-X main.version=${TAG}"
SHELL := /bin/bash
NAMESPACE=leaver-test
VALUES_FILE=helm/leaver/values.yaml

SWAGGER_VERSION=3.23.10

########################## END CONFIG ###########################

clean: stop-docker
	@echo '========================================'
	@echo ' Cleaning project'
	@echo '========================================'
	go clean
	rm -rf build | true
	rm -rf volume | true
	for i in `docker images "${HARBOR_REGISTRY}/${APP}" -q`; do docker rmi -f $$i; done
	@echo 'Done.'

deps:
	@echo '========================================'
	@echo ' Getting Dependencies'
	@echo '========================================'
	@echo 'Cleaning up dependency list...'
	go mod tidy
	@echo 'Downloading dependencies...'
	go mod download
	@echo 'Vendorizing project dependencies...'
	go mod vendor
	@echo 'Done.'

wire: deps
	wire gen cmd/wire.go

build: deps
	@echo '========================================'
	@echo ' Building project'
	@echo '========================================'
	go fmt ./...
	go build -mod=vendor -o build/bin/${APP} ${LDFLAGS} .
	@echo 'Done.'

test-unit:
	@echo '========================================'
	@echo ' Running tests: go test'
	@echo '========================================'
	go test -mod=vendor ./test
	@echo 'Done.'

test-quality:
	@echo '========================================'
	@echo ' Running tests: Lint (golangci-lint)'
	@echo '========================================'
	docker run -it  -v $(PWD):/app --rm golangci/golangci-lint:v1.17.1 bash -c "cd /app && /usr/bin/golangci-lint --verbose -c config/.golangci.yml run"
	@echo NO ERRORS FOUND.

pull-image:
	@echo '========================================'
	@echo ' Getting latest image'
	@echo '========================================'
	docker pull ${BUILD_IMAGE} || true
	@echo 'Done.'

package-image:
	@echo '========================================'
	@echo ' Packaging docker image'
	@echo '========================================'
	docker build -t ${BUILD_IMAGE} .
	@echo 'Done.'

package-chart:
	@echo '========================================'
	@echo ' Packaging chart'
	@echo '========================================'
	mkdir -p build/chart
	cp -r helm/${APP} build/chart
	helm package  --app-version ${APP} -u -d build/chart build/chart/${APP}
	@echo 'Done.'

package: package-image package-chart
	
harbor-login:
	@echo '========================================'
	@echo ' Harbor Login'
	@echo '${HARBOR_REGISTRY}'
	@echo '========================================'
	echo ${HARBOR_PASS} | docker login ${HARBOR_REGISTRY} --username ${HARBOR_USER} --password-stdin
	@echo 'Done.'

publish-image:
	@echo '========================================'
	@echo ' Publishing image'
	@echo '========================================'
	docker push ${BUILD_IMAGE}
	@echo 'Done.'

publish-chart:
	@echo '========================================'
	@echo ' Publishing chart'
	@echo '========================================'
	helm push build/chart/*.tgz tijara
	@echo 'Done.'

publish: publish-image publish-chart

setup-helm: 
	@echo '========================================'
	@echo ' Setting up Helm'
	@echo '========================================'
	helm init --client-only
	helm repo add tijara ${CHART_REPO} --username ${HARBOR_USER} --password ${HARBOR_PASS} 
	helm repo update
	@echo 'Done.'

setup-cluster: 
	kubectl config set-cluster ${CLUSTER_NAME} --server="${CLUSTER_API_URL}"
	kubectl config set clusters.${CLUSTER_NAME}.certificate-authority-data ${CLUSTER_CA}
	kubectl config set-credentials ${CLUSTER_USER} --token="${CLUSTER_USER_TOKEN}"
	kubectl config set-context ${CLUSTER_CONTEXT} --cluster=${CLUSTER_NAME} --user=${CLUSTER_USER}
	kubectl config use-context ${CLUSTER_CONTEXT}
	kubectl config set-context ${CLUSTER_CONTEXT} --namespace=${CLUSTER_NAMESPACE}
	kubectl config view

deploy-chart:
	@echo '========================================'
	@echo ' Deploying application'
	@echo '========================================'
	helm upgrade ${APP} --install \
		--namespace ${NAMESPACE} \
		--set image.tag=${TAG} \
		--set registries[0].url=${HARBOR_REGISTRY} \
		--set registries[0].username=${HARBOR_USER} \
		--set registries[0].password=${HARBOR_PASS} \
		--set extraLabels.git_hash=\"${CI_COMMIT_SHORT_SHA}\" \
		--values ${VALUES_FILE} helm/leaver
	@echo 'Done.'

deploy-chart-aws:
	@echo '========================================'
	@echo ' Deploying application'
	@echo '========================================'
	/home/devs/helm upgrade ${APP} --install \
		--namespace ${NAMESPACE} \
		--set image.tag=${TAG} \
		--set registries[0].url=${HARBOR_REGISTRY} \
		--set registries[0].username=${HARBOR_USER} \
		--set registries[0].password=${HARBOR_PASS} \
		--set extraLabels.git_hash=\"${CI_COMMIT_SHORT_SHA}\" \
		--values ${VALUES_FILE} helm/leaver
	@echo 'Done.'

deploy-chart-manual-uat:
	helm upgrade leaver-uat --install \
		--namespace bdb-uat \
		--set image.tag='test-4578ad2d' \
		--set registries[0].url=harbor.amihan.net \
		--set registries[0].username=amihan-robot \
		--set registries[0].password=AmihanGlobal101 \
		--set extraLabels.git_hash=\"00000001\" \
		--values config/helm-values-uat.yaml helm/leaver
	@echo 'Done.'

delete-chart: 
	@echo '========================================'
	@echo ' Removing application'
	@echo '========================================'
	helm delete --purge ${APP}
	@echo 'Done.'

start:
	@echo '========================================'
	@echo ' Starting application'
	@echo '========================================'
	go run . serve
	@echo 'Done.'

start-docker: stop-docker package-image
	@echo '========================================'
	@echo ' Starting application'
	@echo '========================================'
	docker run -d --name ${APP} -p 8080:8080 ${BUILD_IMAGE}
	@echo 'Done.'

stop-docker:
	@echo '========================================'
	@echo ' Stopping application'
	@echo '========================================'
	docker rm -f ${APP} || true
	@echo 'Done.'

module gitlab.com/jagbay_agsx/leaver.git

go 1.15

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/google/wire v0.5.0
	github.com/gorilla/handlers v1.5.1
	github.com/gorilla/mux v1.8.0
	github.com/lestrrat-go/jwx v1.0.5
	github.com/mitchellh/go-homedir v1.1.0
	github.com/mitchellh/mapstructure v1.4.1
	github.com/sirupsen/logrus v1.7.1
	github.com/slack-go/slack v0.8.1
	github.com/spf13/cobra v1.1.3
	github.com/spf13/viper v1.7.1
	github.com/stretchr/testify v1.6.1 // indirect
	golang.org/x/net v0.0.0-20200226121028-0de0cce0169b
	golang.org/x/oauth2 v0.0.0-20190604053449-0f29369cfe45
	golang.org/x/sys v0.0.0-20210216224549-f992740a1bac // indirect
	golang.org/x/text v0.3.5 // indirect
	google.golang.org/api v0.13.0
	gopkg.in/yaml.v2 v2.4.0
)

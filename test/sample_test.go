package test

import (
	"testing"
)

func TestSample(t *testing.T) {
	value := 1
	if 1 != value {
		t.Errorf("Expected value of 1, but got %v ", value)
	}

}

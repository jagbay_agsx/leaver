# leaver-service

## Prerequisire

Run the following to install dependencies:

```shell
make deps
```

## Run

To run the dependencies (eg. databases, etc):

```
make start
or
go run . serve
```

### Project Structure

```
leaver-service
|- build/               # build artifacts are generated here
|- cmd/                 # command line commands live here. Checkout cobra library
|- config/              # configuration files are here
|- db/                  # for database migration files
|- helm/                # helm chart for kubernetes deployment
|- internal/            # for internal go packages 
| |- domain/            # the actual domains / business logic live here
| |  |-endpoint.go      # endpoints
| |- helper/            # format dates, encrypt/decrypt
| |- middleware/        # mostly used on keycloak, jwt, roles, groups
| |- model/             #
| |- server/            # start or add services
| |- infrastructure/    # the third party infrastructure the service needs (eg. database, etc)
|- .dockerignore        # ignore list for docker
|- .gitignore           # ignore list for git
|- go.mod               # dependencies for project
|- go.sum               # checksum for dependencies, do not manually change
|- main.go              # the main go file
|- Makefile             # build scripts
|- README.md            # this file
```

### Adding Dependencies

To add dependencies, run the following:

```shell
go get -u {dependency}
make deps
```

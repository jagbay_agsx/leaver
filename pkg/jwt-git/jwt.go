package jwt

import (
	"errors"
	"strings"

	"encoding/base64"
	"encoding/json"
)

// UnmarshalClaims unmarshals the claims part of a JSON Web Token into the provided interface
// using encoding/json. This returns errors if the JWT is invalid or unmarshaling fails.
//
// Note: This does not verify if the signature is valid or not.
func UnmarshalClaims(jwt string, out interface{}) error {
	tokens := strings.Split(jwt, ".")
	if len(tokens) != 3 {
		return errors.New("token is not a valid JWT, should have 3 segments")
	}
	claims := tokens[1]
	outBytes, err := base64.RawStdEncoding.DecodeString(claims)
	if err != nil {
		return errors.New("unable to decode token claims")
	}
	if err := json.Unmarshal(outBytes, out); err != nil {
		return errors.New("unable to unmarshal claims to struct: " + err.Error())
	}
	return nil
}

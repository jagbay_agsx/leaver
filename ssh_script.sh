#!/bin/bash

HOST=$1
DIRECTORY_PATH=$2
APP=$3
TAG=$4
HARBOR_USER=$5
HARBOR_PASS=$6
NAMESPACE=$7
VALUES_FILE=$8
COMMIT_HASH=$9

HARBOR_REGISTRY=harbor.amihan.net
AWS_PROMPT='devs@ip-172-28-128-98 ~]$'
PEM_FILE='bdb.pem'

echo "$BDB_AWS" > ${PEM_FILE}
chmod 400 ${PEM_FILE}

#CREATE DIRECTORY REMOTELY
/usr/bin/expect -f - <<EOD
spawn ssh -o StrictHostKeyChecking=no -i ${PEM_FILE} ${HOST}
expect '$AWS_PROMPT'
send "mkdir -p ${DIRECTORY_PATH} || true\r"
expect '$AWS_PROMPT'
EOD

#COPY FILES FOR DEPLOYMENT
scp -o StrictHostKeyChecking=no -i ${PEM_FILE} -r ./helm ${HOST}:${DIRECTORY_PATH}
scp -o StrictHostKeyChecking=no -i ${PEM_FILE} -r ./config ${HOST}:${DIRECTORY_PATH}
scp -o StrictHostKeyChecking=no -i ${PEM_FILE} -r Makefile ${HOST}:${DIRECTORY_PATH}

#DEPLOY SERVICE REMOTELY
/usr/bin/expect -f - <<EOD
spawn ssh -o StrictHostKeyChecking=no -i ${PEM_FILE} ${HOST}
expect '$AWS_PROMPT'
send "kubectl get namespaces\r"
expect '$AWS_PROMPT'
send "cd ${DIRECTORY_PATH}\r"
expect '$AWS_PROMPT'
send "pwd\r"
expect '$AWS_PROMPT'
send "make deploy-chart-aws APP=${APP} TAG=${TAG} HARBOR_USER=${HARBOR_USER} HARBOR_PASS=${HARBOR_PASS} NAMESPACE=${NAMESPACE} VALUES_FILE=config/${VALUES_FILE} CI_COMMIT_SHORT_SHA=${COMMIT_HASH}\r"
expect '$AWS_PROMPT'
EOD



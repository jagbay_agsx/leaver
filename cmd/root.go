package cmd

import (
	"os"
	"strings"

	homedir "github.com/mitchellh/go-homedir"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	cfgFile string
	root    = &cobra.Command{
		Use:   "Leaver Service",
		Short: "Leaver Service",
	}
)

func init() {
	cobra.OnInitialize(initConfig)
	root.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is ${HOME}/.leaver.yaml)")
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		home, err := homedir.Dir()
		if err != nil {
			log.WithError(err).Error("unable to locate home directory for configuration")
			os.Exit(1)
		}

		viper.AddConfigPath(home)
		viper.SetConfigName(".leaver.yaml")
	}

	viper.SetEnvPrefix("LEAVER_SERVICE")
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	viper.AutomaticEnv()

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		log.Println("Using config file:", viper.ConfigFileUsed())
	}
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute(version string) {
	root.Version = version
	if err := root.Execute(); err != nil {
		log.Error("Cannot execute root command: ", err)
		os.Exit(1)
	}
}

package cmd

import (
	"github.com/spf13/viper"
)

var defaults = map[string]interface{}{
	"log.level": "debug",

	// server config
	"api.rest.host": "0.0.0.0",
	"api.rest.port": 8080,
	"api.rest.spec": "./openapi.yaml",

	// API configuration
	"api.rest.cors.allowedOrigins": []string{"*"},
	"api.rest.cors.allowedHeaders": []string{
		"Content-Type",
		"Sec-Fetch-Dest",
		"Referer",
		"accept",
		"Sec-Fetch-Mode",
		"Sec-Fetch-Site",
		"User-Agent",
		"User-Agent",
		"Authorization",
		"API-KEY",
	},
	"api.rest.cors.allowedMethods": []string{
		"OPTIONS",
		"GET",
		"POST",
		"DELETE",
		"PUT",
	},
	"api.rest.rbac": "config/rbac.yaml",

	"secrets.file":  "config/secrets.json",
	"bot.auth":      "xoxb-1756133565794-1766020725316-y3uAKnQNNBYnQlmZGmRYFIpe",
	"bot.channelId": "C01NXUNDW2U",
}

func init() {
	for key, value := range defaults {
		viper.SetDefault(key, value)
	}
}

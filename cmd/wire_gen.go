// Code generated by Wire. DO NOT EDIT.

//go:generate wire
//+build !wireinject

package cmd

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"github.com/slack-go/slack"
	"github.com/spf13/viper"
	bot2 "gitlab.com/jagbay_agsx/leaver.git/internal/components/bot"
	leaver2 "gitlab.com/jagbay_agsx/leaver.git/internal/components/leaver"
	"gitlab.com/jagbay_agsx/leaver.git/internal/entrypoint/api/rest"
	bot3 "gitlab.com/jagbay_agsx/leaver.git/internal/entrypoint/api/rest/bot"
	leaver3 "gitlab.com/jagbay_agsx/leaver.git/internal/entrypoint/api/rest/leaver"
	"gitlab.com/jagbay_agsx/leaver.git/internal/infrastructure/bot"
	"gitlab.com/jagbay_agsx/leaver.git/internal/infrastructure/leaver"
	"golang.org/x/net/context"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/calendar/v3"
	"io/ioutil"
	"net/http"
	"os"
)

// Injectors from wire.go:

func createRestAPI() *rest.API {
	config := ProvideRestAPIConfig()
	router := mux.NewRouter()
	leaverConfig := provideLeaverConfig()
	service := provideLeaverClient()
	repository := leaver.New(leaverConfig, service)
	leaverService := leaver2.NewDefaultService(repository)
	controller := leaver3.NewController(leaverService)
	botConfig := provideSlackBotConfig()
	client := provideSlackBotClient(botConfig)
	botRepository := bot.New(botConfig, client)
	botService := bot2.NewDefaultService(botRepository)
	botController := bot3.NewController(botService)
	api := rest.NewRestAPI(config, router, controller, botController)
	return api
}

// wire.go:

func ProvideRestAPIConfig() *rest.Config {
	var config rest.Config
	err := viper.UnmarshalKey("api.rest", &config)
	if err != nil {
		logrus.WithError(err).Error("unable to read RestAPIConfig")
		os.Exit(1)
	}

	config.Version = "v1"
	rbacData, err := ioutil.ReadFile(config.RBAC)
	if err != nil {
		logrus.WithError(err).Error("Error in reading RBAC data")
	}

	config.RBAC = string(rbacData)
	logrus.Info("========================================")
	logrus.Info("API Configuration")
	logrus.Info("========================================")
	logrus.Info("Host:    ", config.Host)
	logrus.Info("Port:    ", config.Port)
	logrus.Info("Spec:    ", config.Spec)
	logrus.Info("Version: ", config.Port)

	return &config
}

func provideLeaverConfig() *leaver.Config {
	file := viper.GetString("secrets.file")
	jsonFile, err := os.Open(file)
	if err != nil {
		logrus.WithError(err).Error("unable to read security keys file")
		os.Exit(1)
	}
	defer jsonFile.Close()

	byteValue, _ := ioutil.ReadAll(jsonFile)
	var config leaver.Config
	json.Unmarshal(byteValue, &config)
	logrus.Info("========================================")
	logrus.Infof("Calendar Config: %+v", config)
	logrus.Info("========================================")

	return &config
}

func provideLeaverClient() *calendar.Service {
	file := viper.GetString("secrets.file")
	b, err := ioutil.ReadFile(file)
	if err != nil {
		logrus.WithError(err).Errorf("Unable to read client secret file: %v", err)
	}

	calConf, err := google.ConfigFromJSON(b, calendar.CalendarReadonlyScope)
	if err != nil {
		logrus.WithError(err).Errorf("Unable to parse client secret file to calConf: %v", err)
	}
	cal := getClient(calConf)

	srv, err := calendar.New(cal)
	if err != nil {
		logrus.Fatalf("Unable to retrieve Calendar: %v", err)
	}

	return srv
}

func getClient(config *oauth2.Config) *http.Client {
	return config.Client(context.Background(), nil)
}

func provideSlackBotConfig() *bot.Config {
	logrus.Info("Initializing Bot Config")
	conf := &bot.Config{
		Auth:      viper.GetString("bot.auth"),
		ChannelId: viper.GetString("bot.channelId"),
	}

	return conf
}

func provideSlackBotClient(config *bot.Config) *slack.Client {
	logrus.Info("Initializing Bot Client")

	slackApi := slack.New(config.Auth)
	return slackApi
}

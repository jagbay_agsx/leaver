//+build wireinject

package cmd

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"os"

	"github.com/google/wire"
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"github.com/slack-go/slack"
	"github.com/spf13/viper"
	"golang.org/x/net/context"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/calendar/v3"

	"gitlab.com/jagbay_agsx/leaver.git/internal/entrypoint/api/rest"

	// calendar
	"gitlab.com/jagbay_agsx/leaver.git/internal/components/leaver"
	leaverHandlers "gitlab.com/jagbay_agsx/leaver.git/internal/entrypoint/api/rest/leaver"
	leaverInfra "gitlab.com/jagbay_agsx/leaver.git/internal/infrastructure/leaver"

	// slack bot
	"gitlab.com/jagbay_agsx/leaver.git/internal/components/bot"
	botHandlers "gitlab.com/jagbay_agsx/leaver.git/internal/entrypoint/api/rest/bot"
	botInfra "gitlab.com/jagbay_agsx/leaver.git/internal/infrastructure/bot"
)

func createRestAPI() *rest.API {
	wire.Build(
		// calendar
		provideLeaverConfig,
		provideLeaverClient,
		leaver.NewDefaultService,
		leaverInfra.New,
		leaverHandlers.NewController,

		// slackbot
		provideSlackBotConfig,
		provideSlackBotClient,
		bot.NewDefaultService,
		botInfra.New,
		botHandlers.NewController,

		mux.NewRouter,
		ProvideRestAPIConfig,
		rest.NewRestAPI,
	)
	return &rest.API{}
}

func ProvideRestAPIConfig() *rest.Config {
	var config rest.Config
	err := viper.UnmarshalKey("api.rest", &config)
	if err != nil {
		log.WithError(err).Error("unable to read RestAPIConfig")
		os.Exit(1)
	}
	// config.Version = root.Version
	config.Version = "v1"
	rbacData, err := ioutil.ReadFile(config.RBAC)
	if err != nil {
		log.WithError(err).Error("Error in reading RBAC data")
	}

	config.RBAC = string(rbacData)

	log.Info("========================================")
	log.Info("API Configuration")
	log.Info("========================================")
	log.Info("Host:    ", config.Host)
	log.Info("Port:    ", config.Port)
	log.Info("Spec:    ", config.Spec)
	log.Info("Version: ", config.Port)

	return &config
}

func provideLeaverConfig() *leaverInfra.Config {
	file := viper.GetString("secrets.file")
	jsonFile, err := os.Open(file)
	if err != nil {
		log.WithError(err).Error("unable to read security keys file")
		os.Exit(1)
	}
	defer jsonFile.Close()

	byteValue, _ := ioutil.ReadAll(jsonFile)
	var config leaverInfra.Config
	json.Unmarshal(byteValue, &config)

	log.Info("========================================")
	log.Infof("Calendar Config: %+v", config)
	log.Info("========================================")

	return &config
}

func provideLeaverClient() *calendar.Service {
	file := viper.GetString("secrets.file")
	b, err := ioutil.ReadFile(file)
	if err != nil {
		log.WithError(err).Errorf("Unable to read client secret file: %v", err)
	}

	calConf, err := google.ConfigFromJSON(b, calendar.CalendarReadonlyScope)
	if err != nil {
		log.WithError(err).Errorf("Unable to parse client secret file to calConf: %v", err)
	}
	cal := getClient(calConf)

	srv, err := calendar.New(cal)
	if err != nil {
		log.Fatalf("Unable to retrieve Calendar: %v", err)
	}

	return srv
}

func getClient(config *oauth2.Config) *http.Client {
	return config.Client(context.Background(), nil)
}

func provideSlackBotConfig() *botInfra.Config {
	log.Info("Initializing Bot Config")
	conf := &botInfra.Config{
		Auth:      viper.GetString("bot.auth"),
		ChannelId: viper.GetString("bot.channelId"),
	}

	return conf
}

func provideSlackBotClient(config *botInfra.Config) *slack.Client {
	log.Info("Initializing Bot Client")

	slackApi := slack.New(config.Auth)
	return slackApi
}

package leaver

type DefaultService struct {
	repository Repository
}

func NewDefaultService(repository Repository) Service {
	return &DefaultService{
		repository: repository,
	}
}

func (s *DefaultService) Test(value string) (string, error) {
	return s.repository.Test(value)
}

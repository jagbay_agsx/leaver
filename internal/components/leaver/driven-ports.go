package leaver

// Provider is the leaver provider port
type Repository interface {
	Test(value string) (string, error)
}

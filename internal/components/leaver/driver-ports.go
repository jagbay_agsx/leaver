package leaver

// Service is the application port for this component
type Service interface {
	Test(value string) (string, error)
}

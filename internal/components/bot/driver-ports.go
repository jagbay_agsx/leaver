package bot

// Service is the application port for this component
type Service interface {
	Test(value string) (string, error)
	SendMessage(calendarMessage CalendarMessage) (string, error)
}

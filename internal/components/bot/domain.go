package bot

type (
	Test struct {
		Message string `json:"message"`
	}

	CalendarMessage struct {
		Message string `json:"message"`
		Author  string `json:"author"`
	}
)

package bot

// Provider is the leaver provider port
type Repository interface {
	Test(value string) (string, error)
	SendMessage(calendarMessage CalendarMessage) (string, error)
}

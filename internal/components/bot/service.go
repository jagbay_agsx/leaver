package bot

type DefaultService struct {
	repository Repository
}

func NewDefaultService(repository Repository) Service {
	return &DefaultService{
		repository: repository,
	}
}

func (s *DefaultService) Test(value string) (string, error) {
	return s.repository.Test(value)
}

func (s *DefaultService) SendMessage(calendarMessage CalendarMessage) (string, error) {
	return s.repository.SendMessage(calendarMessage)
}

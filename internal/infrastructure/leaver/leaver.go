package leaver

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/jagbay_agsx/leaver.git/internal/components/leaver"
	"google.golang.org/api/calendar/v3"
)

type (
	Config struct {
		Installed struct {
			ClientId                   string   `json:"client_id"`
			ProjectId                  string   `json:"project_id"`
			AuthURI                    string   `json:"auth_uri"`
			TokenURI                   string   `json:"token_uri"`
			AuthProvider_x509_cert_url string   `json:"auth_provider_x509_cert_url"`
			ClientSecret               string   `json:"client_secret"`
			Redirect_uris              []string `json:"redirect_uris"`
		} `json:"installed"`
	}
	Calendar struct {
		config *Config
		client *calendar.Service
	}
)

// calendar
func New(conf *Config, client *calendar.Service) leaver.Repository {
	c := &Calendar{
		config: conf,
		client: client,
	}
	return c
}

func (c *Calendar) Test(value string) (string, error) {
	log.Infof("sample text: %v", value)

	return value, nil
}

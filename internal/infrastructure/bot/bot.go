package bot

import (
	log "github.com/sirupsen/logrus"
	"github.com/slack-go/slack"
	"gitlab.com/jagbay_agsx/leaver.git/internal/components/bot"
)

type (
	Config struct {
		Auth      string `json:"auth"`
		ChannelId string `json:"channelId"`
	}
	Slack struct {
		config *Config
		client *slack.Client
	}
)

// slack bot
func New(conf *Config, client *slack.Client) bot.Repository {
	c := &Slack{
		config: conf,
		client: client,
	}
	return c
}

func (c *Slack) Test(value string) (string, error) {
	log.Infof("sample text: %v", value)

	return value, nil
}

func (c *Slack) SendMessage(calendarMessage bot.CalendarMessage) (string, error) {
	log.Infof("sample text: %+v", calendarMessage)

	channelId := make(chan string)
	go func() {
		channelId <- c.config.ChannelId
	}()
	channel := <-channelId

	message := slack.MsgOptionText(calendarMessage.Message, true)
	_, _, _, err := c.client.SendMessage(channel, message)
	if err != nil {
		log.WithError(err).Error("unable to send slack bot message")
		return "", err
	}

	return calendarMessage.Message, nil
}

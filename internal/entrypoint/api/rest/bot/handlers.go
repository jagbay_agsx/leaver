package bot

import (
	"encoding/json"
	"net/http"

	log "github.com/sirupsen/logrus"

	"gitlab.com/jagbay_agsx/leaver.git/internal/components/bot"
	"gitlab.com/jagbay_agsx/leaver.git/pkg/responses"
)

func testHandler(botService bot.Service) http.HandlerFunc {
	log.Info("handler get slackbot: bot::test-endpoint-bot")

	return func(res http.ResponseWriter, req *http.Request) {
		body := req.Body
		defer body.Close()

		var testing bot.Test
		if err := json.NewDecoder(body).Decode(&testing); err != nil {
			log.WithError(err).Error("unable to read request")
			WriteTestError(res, err)
			return
		}

		botMessage, err := botService.Test(testing.Message)
		if err != nil {
			log.WithError(err).Error("unable to get slackbot")
			WriteTestFunctionError(res, err)
			return
		}

		result := map[string]interface{}{
			"message": botMessage,
		}

		responses.WriteOKWithEntity(res, result)
	}
}

func sendMessage(botService bot.Service) http.HandlerFunc {
	log.Info("handler get slackbot: bot::send-message")

	return func(res http.ResponseWriter, req *http.Request) {
		body := req.Body
		defer body.Close()

		var message bot.CalendarMessage
		if err := json.NewDecoder(body).Decode(&message); err != nil {
			log.WithError(err).Error("unable to read request")
			WriteTestError(res, err)
			return
		}

		botMessage, err := botService.SendMessage(message)
		if err != nil {
			log.WithError(err).Error("unable to get slackbot")
			WriteSendMessageError(res, err)
			return
		}

		result := map[string]interface{}{
			"message": botMessage,
		}

		responses.WriteOKWithEntity(res, result)
	}
}

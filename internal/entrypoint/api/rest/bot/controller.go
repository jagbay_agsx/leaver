package bot

import (
	"net/http"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"

	middleware "gitlab.com/jagbay_agsx/leaver.git/pkg/jwt-auth"
	apikey "gitlab.com/jagbay_agsx/leaver.git/pkg/middlewares"

	"gitlab.com/jagbay_agsx/leaver.git/internal/components/bot"
)

// Controller is the REST controller for auth
type Controller struct {
	botService bot.Service
}

// NewController creates a new Controller
func NewController(botService bot.Service) *Controller {
	return &Controller{
		botService,
	}
}

// Register registers the endpoints to the router
func (c *Controller) Register(router *mux.Router, apiKey string, middleware *middleware.JWT) {
	bot := router.PathPrefix("/bot").Subrouter()
	service := c.botService

	apiKeyMiddleware := apikey.APIKey(apikey.APIKeyTypeHeader, "API-KEY", apiKey)
	bot.Use(apiKeyMiddleware)
	log.Info("API filter enabled on /bot")

	bot.
		Methods(http.MethodPost, http.MethodOptions).
		Path("/test-endpoint-bot").
		HandlerFunc(testHandler(service))
	bot.
		Methods(http.MethodPost, http.MethodOptions).
		Path("/send-message").
		HandlerFunc(sendMessage(service))
}

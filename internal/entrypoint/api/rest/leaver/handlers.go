package leaver

import (
	"encoding/json"
	"net/http"

	log "github.com/sirupsen/logrus"

	"gitlab.com/jagbay_agsx/leaver.git/internal/components/leaver"
	"gitlab.com/jagbay_agsx/leaver.git/pkg/responses"
)

func testHandler(leaverService leaver.Service) http.HandlerFunc {
	log.Info("handler get leaver: user::test-endpoint")

	return func(res http.ResponseWriter, req *http.Request) {
		body := req.Body
		defer body.Close()

		var testing leaver.Test
		if err := json.NewDecoder(body).Decode(&testing); err != nil {
			log.WithError(err).Error("unable to read request")
			WriteTestError(res, err)
			return
		}

		leaverMessage, err := leaverService.Test(testing.Message)
		if err != nil {
			log.WithError(err).Error("unable to get leaver")
			WriteTestFunctionError(res, err)
			return
		}

		result := map[string]interface{}{
			"message": leaverMessage,
		}

		responses.WriteOKWithEntity(res, result)
	}
}

package leaver

import (
	"net/http"

	"gitlab.com/jagbay_agsx/leaver.git/internal/entrypoint/api/rest/common"
)

func WriteTestError(res http.ResponseWriter, err error) {
	status := http.StatusBadRequest
	newErr := common.ErrorBuilder().
		Status(status).
		Title("Test endpoint error").
		Detail(err.Error()).
		Build()
	common.WriteErrorResponse(res, status, newErr)
}

func WriteTestFunctionError(res http.ResponseWriter, err error) {
	status := http.StatusBadRequest
	newErr := common.ErrorBuilder().
		Status(status).
		Title("Test service function error").
		Detail(err.Error()).
		Build()
	common.WriteErrorResponse(res, status, newErr)
}

package leaver

import (
	"net/http"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"

	middleware "gitlab.com/jagbay_agsx/leaver.git/pkg/jwt-auth"
	apikey "gitlab.com/jagbay_agsx/leaver.git/pkg/middlewares"

	"gitlab.com/jagbay_agsx/leaver.git/internal/components/leaver"
)

// Controller is the REST controller for auth
type Controller struct {
	leaverService leaver.Service
}

// NewController creates a new Controller
func NewController(leaverService leaver.Service) *Controller {
	return &Controller{
		leaverService,
	}
}

// Register registers the endpoints to the router
func (c *Controller) Register(router *mux.Router, apiKey string, middleware *middleware.JWT) {
	leaver := router.PathPrefix("/leaver").Subrouter()
	service := c.leaverService

	apiKeyMiddleware := apikey.APIKey(apikey.APIKeyTypeHeader, "API-KEY", apiKey)
	leaver.Use(apiKeyMiddleware)
	log.Info("API filter enabled on /leaver")

	leaver.
		Methods(http.MethodPost, http.MethodOptions).
		Path("/test-endpoint").
		HandlerFunc(testHandler(service))
}

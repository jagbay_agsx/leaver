package rest

import (
	"fmt"
	"net/http"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"

	middleware "gitlab.com/jagbay_agsx/leaver.git/pkg/jwt-auth"
	responses "gitlab.com/jagbay_agsx/leaver.git/pkg/responses"

	"gitlab.com/jagbay_agsx/leaver.git/internal/entrypoint/api/rest/bot"
	"gitlab.com/jagbay_agsx/leaver.git/internal/entrypoint/api/rest/leaver"
)

type (
	Config struct {
		Host    string
		Port    int
		Spec    string
		Version string
		Cors    CORSConfig
		APIKey  string
		JWT     JWT
		RBAC    string
	}

	CORSConfig struct {
		AllowedOrigins []string
		AllowedHeaders []string
		AllowedMethods []string
	}

	API struct {
		config           *Config
		router           *mux.Router
		leaverController *leaver.Controller
		botController    *bot.Controller
	}
	JWT struct {
		PubKey     string
		middleware *middleware.JWT
	}
)

func NewRestAPI(config *Config, router *mux.Router, leaverController *leaver.Controller, botController *bot.Controller) *API {
	return &API{
		config:           config,
		router:           router,
		leaverController: leaverController,
		botController:    botController,
	}
}

func (api *API) Run() error {
	api.router = api.router.PathPrefix("/api/v1").Subrouter()
	api.addMiddlewares()
	api.registerHandlers()
	api.exposeSwagger()
	api.exposeVersion()
	api.enableCORS()
	return http.ListenAndServe(api.address(), api.router)
}

func (api *API) address() string {
	return fmt.Sprintf("%s:%d", api.config.Host, api.config.Port)
}

func (api *API) exposeSwagger() {
	api.router.HandleFunc("/spec", func(res http.ResponseWriter, req *http.Request) {
		http.ServeFile(res, req, api.config.Spec)
	})
	log.Infof("OpenAPI Spec accessible at http://%s/api/v1/spec", api.address())
}

func (api *API) exposeVersion() {
	api.router.HandleFunc("/version", func(res http.ResponseWriter, req *http.Request) {
		responses.WriteOKWithEntity(res, api.config.Version)
	})
}

func (api *API) enableCORS() {
	cors := handlers.CORS(
		handlers.AllowedOrigins(api.config.Cors.AllowedOrigins),
		handlers.AllowedHeaders(api.config.Cors.AllowedHeaders),
		handlers.AllowedMethods(api.config.Cors.AllowedMethods),
	)
	api.router.Use(cors)
	log.Info("CORS filter enabled")
}

func (api *API) addMiddlewares() {
	jwtMiddleware := &middleware.JWT{
		PubKeyURL: api.config.JWT.PubKey,
	}

	err := yaml.Unmarshal([]byte(api.config.RBAC), &jwtMiddleware.RBAC)
	if err != nil {
		log.Errorf("Error decoding RBAC: %v", err.Error())
	}
	api.config.JWT.middleware = jwtMiddleware
}

func (api *API) registerHandlers() {
	api.leaverController.Register(api.router, api.config.APIKey, api.config.JWT.middleware)
	api.botController.Register(api.router, api.config.APIKey, api.config.JWT.middleware)
}

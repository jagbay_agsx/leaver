package common

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/mitchellh/mapstructure"
	log "github.com/sirupsen/logrus"
	"gitlab.com/jagbay_agsx/leaver.git/pkg/middlewares"
)

// WriteErrorResponse is a convenience function for writing error responses
func WriteErrorResponse(res http.ResponseWriter, status int, jsonErrs ...JSONError) {
	res.WriteHeader(status)
	if err := json.NewEncoder(res).Encode(jsonErrs); err != nil {
		log.WithError(err).Error("unable to send error response, the JSON error cound not be encoded")
	}
}

// WriteResponse is a convenience function for writing responses
func WriteResponse(res http.ResponseWriter, status int, entity interface{}) {
	res.WriteHeader(status)
	if err := json.NewEncoder(res).Encode(entity); err != nil {
		log.WithError(err).Error("unable to send response, the entity could not be encoded")
	}
}

func EnableAPIKey(router *mux.Router, apiKey string) {
	apiKeyMiddleware := middlewares.APIKey(middlewares.APIKeyTypeHeader, "API-KEY", apiKey)
	router.Use(apiKeyMiddleware)
}

func WriteActionForbiddenError(res http.ResponseWriter) {
	status := http.StatusForbidden
	newErr := ErrorBuilder().
		Status(status).
		Title("Action Forbidden").
		Detail("403 Forbidden").
		Build()
	WriteErrorResponse(res, status, newErr)
}

// jwt check
type TokenClaim struct {
	BusinessID        int      `json:"businessid,omitempty"`
	PreferredUsername string   `json:"preferred_username"`
	Groups            []string `json:"groups"`
	BranchID          int      `json:"branchid"`
}

func GetTokenUsername(req *http.Request) string {
	claims := req.Context().Value("claims")

	var tokenClaim TokenClaim

	//mapstructure can't read keys with underscore. need to define TagName
	config := &mapstructure.DecoderConfig{
		Metadata: nil,
		Result:   &tokenClaim,
		TagName:  "json",
	}

	decoder, err := mapstructure.NewDecoder(config)
	if err != nil {
		panic(err)
	}

	err = decoder.Decode(claims)
	if err != nil {
		panic(err)
	}

	return tokenClaim.PreferredUsername
}

func CheckUserAccess(req *http.Request, username string) bool {
	claims := req.Context().Value("claims")

	var tokenClaim TokenClaim

	//mapstructure can't read keys with underscore. need to define TagName
	config := &mapstructure.DecoderConfig{
		Metadata: nil,
		Result:   &tokenClaim,
		TagName:  "json",
	}

	decoder, err := mapstructure.NewDecoder(config)
	if err != nil {
		panic(err)
	}

	err = decoder.Decode(claims)
	if err != nil {
		panic(err)
	}

	if tokenClaim.PreferredUsername != username {
		return false
	}

	return true
}

// check if admin, org-admin, and branch-admin have access
func CheckAdminAccess(req *http.Request) bool {
	claims := req.Context().Value("claims")

	var tokenClaim TokenClaim
	mapstructure.Decode(claims, &tokenClaim)

	for _, group := range tokenClaim.Groups {
		if group == "safepass-admin" || group == "super-admin" {
			return true
		}
	}
	return false
}

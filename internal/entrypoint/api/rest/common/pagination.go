package common

type Pagination struct {
	Page        int `json:"page"`          // page number
	RowsPerPage int `json:"rows_per_page"` //
	TotalRows   int `json:"total_rows"`
}

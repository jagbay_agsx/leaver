package common

import (
	"bytes"
	"encoding/json"
	"errors"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"strings"

	log "github.com/sirupsen/logrus"
)

type Error struct {
	Message string `json:"message"`
}

// Error implements the Error interface, returns the error
// message
func (me Error) Error() string {
	return me.Message
}

// RequestBuilder is a builder for a request to the Mifos API.
type RequestBuilder struct {
	httpClient  *http.Client
	baseURL     string
	method      string
	path        string
	headers     map[string]string
	queryParams map[string]string
	body        interface{}
	rawBody     []byte
	file        *os.File
	username    string
	password    string
}

func Request(httpClient *http.Client, url string) *RequestBuilder {
	reqBuilder := &RequestBuilder{
		httpClient: httpClient,
		baseURL:    url,
		headers: map[string]string{
			"Content-Type": "application/json",
		},
		queryParams: map[string]string{},
	}

	return reqBuilder
}

// UseBasicAuth enables basic authentication to be added to the header of this request
func (mrb *RequestBuilder) UseBasicAuth(username, password string) *RequestBuilder {
	mrb.username = username
	mrb.password = password
	return mrb
}

// Get is a convenience method for a GET request
func (mrb *RequestBuilder) Get(path string) *RequestBuilder {
	mrb.method = http.MethodGet
	mrb.path = path
	return mrb
}

// Post is a convenience method for a POST request
func (mrb *RequestBuilder) Post(path string) *RequestBuilder {
	mrb.method = http.MethodPost
	mrb.path = path
	return mrb
}

// Delete is a convenience method for a DELETE request
func (mrb *RequestBuilder) Delete(path string) *RequestBuilder {
	mrb.method = http.MethodDelete
	mrb.path = path
	return mrb
}

// Patch is a convenience method for a PATCH request
func (mrb *RequestBuilder) Patch(path string) *RequestBuilder {
	mrb.method = http.MethodPatch
	mrb.path = path
	return mrb
}

// Put is a convenience method for a PUT request
func (mrb *RequestBuilder) Put(path string) *RequestBuilder {
	mrb.method = http.MethodPut
	mrb.path = path
	return mrb
}

// Method sets the method for this request. Convenience methods Post, Get, Delete
// Patch, and Put are available and might be preferred.
func (mrb *RequestBuilder) Method(method string) *RequestBuilder {
	mrb.method = method
	return mrb
}

// Path sets the path of the request. The convenience methods Post, Get, Delete
// Patch, and Put also allows the setting of the path.
func (mrb *RequestBuilder) Path(path string) *RequestBuilder {
	mrb.path = path
	return mrb
}

// AddHeader adds a new header to this request.
func (mrb *RequestBuilder) AddHeader(key, value string) *RequestBuilder {
	mrb.headers[key] = value
	return mrb
}

// AddQueryParam adds a query paramerter for this request.
func (mrb *RequestBuilder) AddQueryParam(key, value string) *RequestBuilder {
	mrb.queryParams[key] = value
	return mrb
}

// SetQueryParams sets the query parameters for this requests. This will replace
// any existing query parameters that were previously set.
func (mrb *RequestBuilder) SetQueryParams(params map[string]string) *RequestBuilder {
	mrb.queryParams = params
	return mrb
}

// Body sets the request body, this should be a type that can be marshaled as JSON.
// If this is set, RawBody will be cleared.
func (mrb *RequestBuilder) Body(body interface{}) *RequestBuilder {
	mrb.body = body
	mrb.rawBody = nil
	return mrb
}

// RawBody sets the RawBody as a byte slice. If this is set, Body will be cleared.
func (mrb *RequestBuilder) RawBody(body []byte) *RequestBuilder {
	mrb.rawBody = body
	mrb.body = nil
	return mrb
}

func (mrb *RequestBuilder) File(file *os.File) *RequestBuilder {
	mrb.rawBody = nil
	mrb.body = nil
	mrb.file = file
	return mrb
}

// Call will construct the request and call the Mifos API. The response will be
// extracted and wrapped as a Response and the connection will be closed.
func (mrb *RequestBuilder) Call() (*Response, error) {
	// create full URL
	url := strings.Join([]string{mrb.baseURL, mrb.path}, "/")

	log.Printf("URL: %v", url)
	// convert body to JSON if body is provided
	body, err := entityToReader(mrb.body)
	if err != nil {
		log.WithError(err).Error("body cannot be converted to JSON")
		return nil, err
	}

	// if not JSON body is provided, check raw body in bytes
	if body == nil {
		body = bytesToReader(mrb.rawBody)
	}

	// create the initial request
	req, err := http.NewRequest(mrb.method, url, body)
	if err != nil {
		log.WithError(err).Error("unable to create new request")
		return nil, err
	}

	// set username & password if they are defined
	if mrb.username != "" && mrb.password != "" {
		req.SetBasicAuth(mrb.username, mrb.password)
	}

	// add headers
	for k, v := range mrb.headers {
		req.Header.Add(k, v)
	}

	// add query parameters
	if len(mrb.queryParams) > 0 {
		q := req.URL.Query()
		for k, v := range mrb.queryParams {
			q.Add(k, v)
		}
		req.URL.RawQuery = q.Encode()
	}
	res, err := mrb.httpClient.Do(req)

	if err != nil {
		log.WithError(err).Error("unable to call API")
		return nil, err
	}

	code := res.StatusCode
	resBody := res.Body
	defer resBody.Close()

	rawBody, err := ioutil.ReadAll(resBody)
	if err != nil {
		log.WithError(err).Error("unable to read response body")
		return nil, err
	}

	return &Response{
		statusCode: code,
		raw:        rawBody,
		header:     res.Header,
	}, nil
}

func (mrb *RequestBuilder) FileCall() (*Response, error) {
	url := strings.Join([]string{mrb.baseURL, mrb.path}, "/")

	req, err := http.NewRequest(mrb.method, url, mrb.file)
	if err != nil {
		log.WithError(err).Error("unable to create new request")
		return nil, err
	}

	if mrb.username != "" && mrb.password != "" {
		req.SetBasicAuth(mrb.username, mrb.password)
	}

	// add headers
	for k, v := range mrb.headers {
		req.Header.Add(k, v)
	}

	res, err := mrb.httpClient.Do(req)

	if err != nil {
		log.WithError(err).Error("unable to call API")
		return nil, err
	}

	code := res.StatusCode
	resBody := res.Body
	defer resBody.Close()

	rawBody, err := ioutil.ReadAll(resBody)
	if err != nil {
		log.WithError(err).Error("unable to read response body")
		return nil, err
	}

	return &Response{
		statusCode: code,
		raw:        rawBody,
		header:     res.Header,
	}, nil
}

// Response is a response from the Mifos API.
type Response struct {
	statusCode int
	raw        []byte
	header     map[string][]string
}

// Success returns true if the call to the Mifos API was successful. This means
// that the API responded with either HTTP 200 or HTTP 201.
func (mr *Response) Success() bool {
	return mr.statusCode == http.StatusOK || mr.statusCode == http.StatusCreated || mr.statusCode == http.StatusAccepted || mr.statusCode == http.StatusNoContent
}

// Raw returns the raw response as byte slice.
func (mr *Response) Raw() []byte {
	return mr.raw
}

// AsEntity will unmarshal the response from JSON into the entity provided.
func (mr *Response) AsEntity(entity interface{}) error {
	err := json.Unmarshal(mr.raw, entity)
	if err != nil {
		log.WithError(err).Error("unable to convert response JSON to entity")
	}
	return err
}

// AsString returns the response as a string.
func (mr *Response) AsString() string {
	return string(mr.raw)
}

// AsError returns the response as a string wrapped as an error.
func (mr *Response) AsError() error {
	return errors.New(mr.AsString())
}

// AsCode returns the status code
func (mr *Response) AsCode() int {
	return mr.statusCode
}

// AsLogField returns the status code as "status-code", {statusCode}. Useful for
// logging.
func (mr *Response) AsLogField() (string, int) {
	return "status-code", mr.statusCode
}

func entityToReader(entity interface{}) (io.Reader, error) {
	if entity == nil {
		return nil, nil
	}
	data, err := json.Marshal(entity)
	if err != nil {
		log.Error("unable to marshal entity", err)
		return nil, &Error{
			Message: err.Error(),
		}
	}
	log.Debug("Body Entity: ", string(data))
	return bytes.NewReader(data), nil
}

func bytesToReader(b []byte) io.Reader {
	if b == nil {
		return nil
	}
	return bytes.NewReader(b)
}

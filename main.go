package main

import "gitlab.com/jagbay_agsx/leaver.git/cmd"

var version = "dev"

func main() {
	cmd.Execute(version)
}
